import java.util.Arrays;
import java.util.Random;

public class Main {

    public static int sumDiapazoneArray(int from, int to) {
        int sum = 0;
        int c = (to - from)+1;
        int[] array = new int[c];
        if (from > to){
            return - 1;
        }
        for (int i = 0; i < c; i++) {
            array[i] = from++;
            sum = sum + array[i];
        }
        return sum;
    }

    public static void evenList(){
        int[] arr = createArray(8,6);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0){
                System.out.println("Четные числа массива " + " " + arr[i]);
            }
        }
    }

    public static int[] createArray(int bound, int arrayLength){
        Random random = new Random();
        int[] newArray = new int[arrayLength];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = random.nextInt(bound);
        }
        System.out.println(Arrays.toString(newArray));
        return newArray;
    }

    public static int convertToInt(){

        int [] array = createArray(5, 3);
        String number = Arrays.toString(array);
        String n = number.replace(",","");
        String n1 = n.replace("[","");
        String n2 = n1.replace("]","");
        String n3 = n2.replace(" ","");
        int num = Integer.parseInt(n3);
        System.out.println(num);
        return num;
    }

    public static void main(String[] args) {
        int summa = convertToInt();
    }
}