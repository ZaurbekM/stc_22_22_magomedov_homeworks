import java.util.List;

public interface ProductsRepository {
    List<Product> findAllByTitleLike(String title);

    Product findById(Integer id);

    void update(Product product);
}
