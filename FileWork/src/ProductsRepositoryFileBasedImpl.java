import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository {
    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringProduct = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String title = parts[1].toLowerCase();
        Double price = Double.parseDouble(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, title, price, count);
    };

    private static final Function<Product, String> productStringFunction = product ->
            product.getTitle() + "|" + product.getPrice() + "|" + product.getCount();

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringProduct)
                    .filter(product -> product.getTitle().toLowerCase().contains(title))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Product findById(Integer id) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringProduct)
                    .filter(product -> product.getId().equals(id))
                    .min(Comparator.comparingInt(Product::getId))
                    .stream().findFirst()
                    .orElse(null);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String productToSave = productStringFunction.apply(product);
            bufferedWriter.write(productToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}


