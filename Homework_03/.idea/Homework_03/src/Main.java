import java.util.Arrays;
import java.util.Random;

public class Main {

    public static int sizeArray(int[]array){
        int size = array.length;
        return size;
    }

    public static int[] createArray(int bound, int arrayLength){
        Random random = new Random();
        int[] newArray = new int[arrayLength];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = random.nextInt(bound);
        }
        System.out.println(Arrays.toString(newArray));
        return newArray;
    }

    public static void main(String[] args) {
        int countLocalMin = 0;

        int [] arr = createArray( 9, 7);

        if (arr[0] < arr[1])
            countLocalMin++;

        for (int i = 1; i < arr.length-1; i++) {
            if (arr[i] < arr[i + 1] && arr[i] < arr[i - 1]) {
                countLocalMin++;
            }
        }
        if (arr[arr.length - 2] > arr[arr.length-1])
            countLocalMin++;



        System.out.println(countLocalMin);

    }
}